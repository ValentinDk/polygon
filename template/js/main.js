$(document).ready(function() {
    let color = $("#color").text();
    let cp = ColorPicker(document.getElementById('pcr'), document.getElementById('picker'),
        function(hex, hsv, rgb, mousePicker, mousepcr) {
            $("#color").text(hex);
            ColorPicker.positionIndicators(
                document.getElementById('pcr-indicator'),
                document.getElementById('picker-indicator'),
                mousepcr, mousePicker);

            document.getElementsByTagName('body')[0].style.backgroundColor = hex;
        });
    cp.setHex(color);
});

function funcBefore() {
    $("#information").text("Сохраняется...");
}

function funcSuccess() {
    $("#information").hide();
    alert("Сохранено");
}

$(document).ready(function() {
    $("#load").bind("click", function() {
        $.ajax({
            url: '/color/',
            type: "POST",
            data: ({color: $("#color").text()}),
            dataType: "html",
            beforeSend: funcBefore,
            success: funcSuccess,
        });
    });
});