<?php

use Components\Router;

error_reporting(E_ALL);
ini_set('display_errors', 1);

define('ROOT', dirname('__FILE__'));

include_once(ROOT.'/vendor/autoload.php');

session_start();

$route = new Router();
$route->run();