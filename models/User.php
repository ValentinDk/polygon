<?php

namespace Models;

use Components\Database;

Class User
{
    /**
     * @param string $email
     * @param string $password
     * @return bool|array
     */
    public static function verificationUser(string $email, string $password)
    {
        $db = Database::getConnection();
        $query = $db->prepare('SELECT * FROM users WHERE email = :email AND password = :password');
        $query->execute(['email' => $email, 'password' => $password]);
        $user = $query->fetch();

        return $user ? $user : false;
    }

    /**
     * @param string $color
     * @param int $id
     * @return bool
     */
    public static function setColor(string $color, int $id): bool
    {
        $db = Database::getConnection();
        $query = $db->prepare('UPDATE users SET bg_color = :color WHERE id = :id');
        $result = $query->execute(['color' => $color, 'id' => $id]);

        return $result;
    }

    /**
     * @param int $id
     * @return string
     */
    public static function getColor(int $id): string
    {
        $db = Database::getConnection();
        $query = $db->prepare('SELECT bg_color FROM users WHERE id = :id');
        $query->execute(['id' => $id]);
        $color = $query->fetch();

        return $color['bg_color'];
    }
}