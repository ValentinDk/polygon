<?php

namespace Controllers;

use Components\BaseController;
use Models\User;

Class SiteController extends BaseController
{
    public function actionAuth()
    {
        $errors= [];

        if (isset($_POST['submit'])) {
            $password = $_POST['password'];
            $email = $_POST['email'];
            $user = User::verificationUser($email, $password);

            if ($user) {
                $_SESSION['user'] = $user;
                header('Location: /');
            } else {
                $errors[] = 'Неверено введён логин либо пароль!';
            }
        }

        $errorsView = $this->view->fetchPartial('layouts/errors',['errors' => $errors]);
        $this->view->render('autorization', ['errors' => $errorsView]);

        return true;
    }

    public function actionIndex()
    {
        $bgColor = User::getColor($_SESSION['user']['id']);

        $this->view->render('index', ['color' => $bgColor]);

        return true;
    }

    public function actionGetColor()
    {
        if (isset($_POST['color'])) {
            $bgColor = $_POST['color'];
            User::setColor($bgColor, $_SESSION['user']['id']);
        }

        return true;
    }
}
