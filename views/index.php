<div id="color-picker" class="cp-default">
    <div class="picker-wrapper">
        <div id="picker" class="picker"></div>
        <div id="picker-indicator" class="picker-indicator"></div>
    </div>
    <div class="pcr-wrapper">
        <div id="pcr" class="pcr"></div>
        <div id="pcr-indicator" class="pcr-indicator"></div>
    </div>
    <button type="submit" name="submit" id="load" class="btn btn-warning">Сохранить</button>
    <div id="information"></div>
    <div id="color">
        <?= $color; ?>
    </div>
</div>